// alert("Hello");

//gets input from form that user inputs
function getTotal(){
  //Gets value of bill amount entered in to input field
  var bill = parseFloat(document.getElementById("bill").value);
  console.log(bill);

  //Gets the value of tip percentage
  var tip = parseFloat(document.getElementById("tip").value) / 100;
  // tip = tip.toFixed(2);
  console.log(tip);

  // calls function to calculate Total
  tipFinal = calculateTip(bill, tip);
  console.log("tip final " + tipFinal);

  billTotal = bill + parseFloat(tipFinal);
  billTotal = billTotal.toFixed(2);

  //updates page elementys to show tip amount and total to be paid
  document.getElementById("tipAmount").innerHTML = ("Tip Amount $" + tipFinal);
  document.getElementById("billTotal").innerHTML =("Bill total $" + billTotal);
  // showResults();
}

//Calculates tip amount for user
function calculateTip (bill, tip){
  var tipTotal = bill * tip;
  tipFinal = tipTotal.toFixed(2);

  return tipFinal;
}

function showResults(){
  var show = document.getElementById("result");
  show.style.visibility = "visible";

  console.log(show);
}
